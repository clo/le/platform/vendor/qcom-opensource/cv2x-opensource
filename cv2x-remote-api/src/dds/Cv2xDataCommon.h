/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef _CV2XDATACOMMON_H__
#define _CV2XDATACOMMON_H__

#define UD_SOCK_SERVER_ID  0
#define UD_SOCK_CLIENT_ID  1
#define CV2X_DOMAIN_ID     1
#define CV2X_BUFFER_SIZE   8192
#include <telux/cv2x/Cv2xRadio.hpp>
#endif //_CV2XDATACOMMON_H__
